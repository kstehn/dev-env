FROM electronuserland/builder:wine
LABEL author="Kevin Stehn"
LABEL email="docker@kstehn.de"
LABEL version="1.0"
LABEL description="Basic Development Image. Build Clojure/Script Projects and Electron projects."

# Install make and npm, also zip and unzip just in case
RUN apt-get update \
    && apt-get install -y curl software-properties-common \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get install -y make nodejs zip unzip wget \
    && npm install -g electron-packager

# Install OpenJDK 11
RUN wget -O /tmp/openjdk.tar.gz https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.3%2B7/OpenJDK11U-jdk_x64_linux_hotspot_11.0.3_7.tar.gz \
    && mkdir -p /usr/lib/jvm/ \
    && tar xzvf /tmp/openjdk.tar.gz -C /usr/lib/jvm/ \
    && rm -rf openjdk.tar.gz

ENV PATH /usr/lib/jvm/jdk-11.0.3+7/bin:$PATH

# Install Leiningen
RUN wget -O /usr/bin/lein https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein \
    && chmod +x /usr/bin/lein \
    && lein self-install

# Install Boot
ENV BOOT_VERSION=2.8.2
ENV BOOT_INSTALL=/usr/local/bin/
ENV BOOT_AS_ROOT yes

WORKDIR /tmp

RUN mkdir -p $BOOT_INSTALL \
    && wget -q https://github.com/boot-clj/boot-bin/releases/download/latest/boot.sh \
    && echo "Comparing installer checksum..." \
    && echo "f717ef381f2863a4cad47bf0dcc61e923b3d2afb *boot.sh" | sha1sum -c - \
    && mv boot.sh $BOOT_INSTALL/boot \
    && chmod 0755 $BOOT_INSTALL/boot

ENV PATH=$PATH:$BOOT_INSTALL

RUN boot

WORKDIR /project