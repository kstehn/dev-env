# Development Environment
Docker Image to build clojure/clojurescript projects.

Commands installed:

* npm
* java 11
* lein
* boot
* curl and wget
* electron-packager
* wine
* make

Basic usage:

```
$ docker run --rm -v "$(pwd):/project"  -it kstehn/dev-env:latest
```

Execute this in your project folder.
After that you can build it like you normaly would.

You can also add your local .m2 repository just add to the basic command

```
-v "REPLACE:/root/.m2"
```
`REPLACE` should point to your .m2 repository.